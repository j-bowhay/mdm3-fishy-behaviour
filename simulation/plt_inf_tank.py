#!/usr/bin/env python3

import matplotlib.pyplot as plt
from numpy import gradient

from fish import Zebrafish, FishTank

def main():
    # Simulation parameters
    TANK_SIZE = 5
    FISH_SPEED = 1
    DETECTION_RADIUS = 5
    ETA = 0.5
    DELTA_T = 1
    N = 100
    
    # Simulation objects
    fish_1 = Zebrafish(FISH_SPEED, DETECTION_RADIUS, ETA)
    fish_2 = Zebrafish(FISH_SPEED, DETECTION_RADIUS, ETA)

    tank = FishTank(TANK_SIZE, 1)
    tank.add_fish(fish_1)
    tank.add_fish(fish_2)
    tank.seed_fishes_randomly()


    #Run Simulation
    for _ in range(N):
        tank.step(DELTA_T)

    
    # Plot results
    fig_pos, ax_pos = plt.subplots()
    fig_angle, ax_angle = plt.subplots()
    for fish in tank.fishes:
        ax_pos.plot(fish.x_trajectory, fish.y_trajectory, '-x')
        ax_pos.plot(fish.x_trajectory[0], fish.y_trajectory[0], 'ro', markersize=20)

        # Calculate and plot angular velocity for each fish
        angular_velocity = gradient(fish.theta_trajectory)
        ax_angle.plot(angular_velocity)

    plt.show()

if __name__ == '__main__':
    main()
