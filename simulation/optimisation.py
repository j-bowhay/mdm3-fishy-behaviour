import time
from datetime import datetime
import math
from random import random

from pathos.multiprocessing import Pool
from mpl_toolkits.mplot3d import Axes3D  
import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize

from fish import Zebrafish, SqrtFishTank


def sqrt_sim(r: float, e: float) -> tuple:
    # Simulation parameters
    TANK_SIZE = 90
    FISH_SPEED = 10.26096
    DETECTION_RADIUS = r
    ETA = e
    DELTA_T = 0.0333
    N = int(1200 // DELTA_T)
    
    # Simulation objects
    fish_1 = Zebrafish(FISH_SPEED, DETECTION_RADIUS, ETA)
    fish_2 = Zebrafish(FISH_SPEED, DETECTION_RADIUS, ETA)

    tank = SqrtFishTank(TANK_SIZE)
    tank.add_fish(fish_1)
    tank.add_fish(fish_2)

    # Seed fish so that they are already within detection radius and aligned
    radius = [r * math.sqrt(random()) for _ in range(2)]
    theta = [random() * math.tau for _ in range(2)]
    x = [0.5 * TANK_SIZE + radius[i] * math.cos(theta[i]) for i in range(2)]
    y = [0.5 * TANK_SIZE + radius[i] * math.sin(theta[i]) for i in range(2)]
    tank.seed_fish_manually(x, y, [theta[0], theta[0]])

    #Run Simulation
    for _ in range(N):
        tank.step(DELTA_T)

    for fish in tank.fishes:
        # Calculate and plot angular velocity for each fish
        angular_velocity = np.gradient(fish.theta_trajectory)

    # Calculate RMS angular velocity
    rms = math.sqrt(np.mean(np.square(angular_velocity)))
    
    # Calculate order/alignment
    fish_1_angle = np.array(fish_1.theta_trajectory)
    fish_2_angle = np.array(fish_2.theta_trajectory)

    fish_1_vel = np.column_stack((np.cos(fish_1_angle), np.sin(fish_1_angle)))
    fish_2_vel = np.column_stack((np.cos(fish_2_angle), np.sin(fish_2_angle)))

    order = np.mean(0.5 * np.linalg.norm(fish_1_vel + fish_2_vel, axis=1))

    # create position matrix for each fish
    fish1_pos = np.column_stack((fish_1.x_trajectory, fish_1.y_trajectory))
    fish2_pos = np.column_stack((fish_2.x_trajectory, fish_2.y_trajectory))

    # find x,y distance between
    dr = np.abs(np.subtract(fish1_pos, fish2_pos))
    # shift distances to account for torus effect
    to_shift = np.greater(dr, TANK_SIZE/2)
    dr[to_shift] = TANK_SIZE - dr[to_shift]
    # find mean of euclidean norm
    distance = np.mean(np.linalg.norm(dr, axis=1))
    # Return RMS Angular velocity and order
    return rms, order, distance


def objective_func(x: list) -> float:
    # run simualation 50 times with given parameters
    data = p.map(lambda _ : sqrt_sim(x[0], x[1]), range(50))

    # extracted data from returned values
    rms = np.mean([x[0] for x in data])
    order = np.mean([x[1] for x in data])
    distance = np.mean([x[2] for x in data])

    # return objective func
    return (10*abs(rms - 0.0737389) + abs(order - 0.878338) + 
        0.01*abs(distance - 6.569559))


def generate_surface(bounds: list) -> tuple:
    print('generating surface')
    radius = np.linspace(bounds[0][0], bounds[0][1], 15)
    eta = np.linspace(bounds[1][0], bounds[1][1], 15)
    XX, YY = np.meshgrid(radius, eta, indexing='ij')
    objective_func_surface = np.zeros((radius.size, eta.size))

    start = time.time()
    # Generate objective function surface
    for i, r in enumerate(radius):
        for j, e in enumerate(eta):
            objective_func_surface[i, j] = objective_func([r, e])
            print(i, j)
    # timing
    print(abs(start - time.time()))
    return XX, YY, objective_func_surface


def plt_surface(XX: np.array, YY: np.array, surface):
    # Plot objective function surface
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(XX, YY, surface, cmap='terrain')
    ax.view_init(45,-45)
    ax.set_xlabel('Detection Radius (cm)')
    ax.set_ylabel('Random Pertubation')
    ax.set_zlabel('Objective Function')



def optimize_methods(bounds: list) -> dict:
    print('Running Optimisation')
    results = dict()
    # run optimisation routine
    results['shgo'] = optimize.shgo(objective_func, bounds, n=30,
        sampling_method='sobol')
    print('shgo complete')
    print(results['shgo'])
    # save results
    write_output(results['shgo'])
    return results


def write_output(output):
    with open('results.txt','a') as f:
        f.write(datetime.now().strftime("%d/%m/%Y %H:%M:%S")+"\n")
        f.write(str(output)+'\n')


def main():
    global p
    p = Pool()

    # Detection radius must be in [0,90] and random purtibation [0,1]
    bounds = [(0,90),(0,1)]
    optimize_methods(bounds)

    XX, YY, surface = generate_surface(bounds)
    plt_surface(XX, YY, surface)
    plt.show()


if __name__ == '__main__':
    main()