# Simulation

## Dependencies
* `NumPy`
* `SciPy`
* `Matplotlib`
* `Pathos`

## Simulation Objects
`fish.py` contains all the classes required to create a zebrafish simulation. Please see the docstring of each class for a more detailed explanation.

### `ZebraFish`
This class encapsulates a zebrafish behaving according to the Vicsek Model [1]. Before use, it needs to be placed in a `FishTank` and spawned with an initial position and direction.

### `FishTank`
This class is used to store `ZebraFish` and step the simulation. This tank does not have a boundary.

### `SqrtFishTank`

Inherits the `FishTank` however has a square boundary. When `ZebraFish` leave the boundary they are transported to the other side of the tank. This can effectively be considered as a tank on a torus.

### `VisualSqrtFishTank`

The same as the `SqrtFishTank` however can be added to a figure as a `Matplotlib` patch.

## Minimum Working Example

```
TANK_SIZE = 5
FISH_SPEED = 1
DETECTION_RADIUS = 5
ETA = 0.5
DELTA_T = 1
N = 100
    
# Simulation objects
fish_1 = Zebrafish(FISH_SPEED, DETECTION_RADIUS, ETA)
fish_2 = Zebrafish(FISH_SPEED, DETECTION_RADIUS, ETA)

tank = FishTank(TANK_SIZE, 1)
tank.add_fish(fish_1)
tank.add_fish(fish_2)
tank.seed_fishes_randomly()

#Run Simulation
for _ in range(N):
    tank.step(DELTA_T)
```

## Trajectory examples
`plt_inf_tank.py` and `plt_sqrt_tank.py` both plot example trajectories in an infinite tank and finite square tank respectively.

## Swarming animation
`swarm_animation.py` generates a quiver plot animation of Zebrafish following the Vicsek model based on the given parameters.

## Parameter Optimisation
`optimisation.py` runs the optimisation routine to find the optimal parameters to match the data and then saves the results to `results.txt`. To validate the optimisation the objective function is also plotted as a surface so that these results can be visually checked.



## References

[1] Vicsek, Tamás et al. "Novel Type of Phase Transition in a System of Self-Driven Particles". Physical Review Letters 75. 6(1995): 1226–1229.