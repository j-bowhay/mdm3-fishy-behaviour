from math import sin, cos, tau, pi, sqrt, atan2
from statistics import mean
from random import random, seed

from matplotlib.patches import Rectangle, Circle


class Zebrafish:
    def __init__(self, speed: float, radius: float, noise: float) -> None:
        """Zebrafish class for use in simulations. Behavour based on the 
        Vicsek model (https://en.wikipedia.org/wiki/Vicsek_model).

        Zebbrafish must be seeded with a position and angle using the 
        seed_fish() method first.

        Args:
            speed (float): the speed of the fish
            radius (float): the interaction radius r
            noise (float): the level of random purtibation eta
        """
        self.speed = speed
        self.detection_radius = radius
        self.noise = noise

        # These attributes are to be determined during seeding
        self._x = 0
        self._y = 0
        self._theta = None
        self.x_trajectory = []
        self.y_trajectory = []
        self.theta_trajectory = []
        self.tank = None

    # Define getters and setters for x, y and theta attribute so that 
    # when a position is set it is automatically added to the trajectory list
    # of all previous values

    @property
    def x(self) -> float:
        return self._x

    @x.setter
    def x(self, x: float) -> None:
        self._x = x
        self.x_trajectory.append(x)

    @property
    def y(self) -> float:
        return self._y

    @y.setter
    def y(self, y: float) -> None:
        self._y = y
        self.y_trajectory.append(y)

    @property
    def theta(self) -> float:
        return self._theta

    @theta.setter
    def theta(self, theta: float) -> None:
        self._theta = theta
        self.theta_trajectory.append(theta)

    def seed_fish(self, x: float, y:float, angle:float, tank) -> None:
        """Set the x,y position of the fish and the direction it is travelling 
        in as well as the tank object it is stored in

        Args:
            x (float): fish's x coordinate
            y (float): fish's y coordinate
            angle (float): fish's initial direction of travel
            tank (FishTank): the fish tank that the fish is stored in
        """
        self.x = x
        self.y = y
        self.theta = angle
        self.tank = tank

    def adjust_course(self) -> None:
        """Adjust the direction of the fish is there are any other fish in the 
        fish tank within the detection radius
        """
        # Default angle if no fish are in range
        mean_theta = self.theta
        
        for fish in self.tank.fishes:
            # Make sure not to compare a fish to itself
            if fish != self:
                # mean angle of the two fish if within range of each other
                dx = abs(self.x - fish.x)
                dy = abs(self.y - fish.y)
                # account for distance on a torus
                if self.tank.torus:
                    if dx > 0.5 * self.tank.spawn_area:
                        dx = self.tank.spawn_area - dx
                    if dy > 0.5 * self.tank.spawn_area:
                        dy = self.tank.spawn_area - dx    
                distance = sqrt(dx**2 + dy**2)
                if distance < self.detection_radius:
                    mean_theta = mean([self.theta, fish.theta])
        
        # Calculate new direction based on equation given by vicsek
        self.theta_new = mean_theta + (random() - 0.5) * self.noise
        # Keep direction in [0,2pi] to prevent mean being messed up
        self.theta_new %= tau

    def step(self, step_size: float) -> None:
        """Steps the fish a given time step along the simulation

        Args:
            step_size (float): time to step fish through
        """
        self.theta = self.theta_new
        # Set current position as new position
        x, y = self._calc_new_pos(step_size)
        self.x = x
        self.y = y

    def _calc_new_pos(self, step_size: float) -> tuple:
        """Private method to calculate the fish's next position after a given 
        step

        Args:
            step_size (float): the time step to calculate the new position at

        Returns:
            tuple(x, y):
                x (float): new x coordinate
                y (float): new y coordinate
        """
        # Calculate new position using equation given by vicsek
        x = self.x + step_size * self.speed * cos(self.theta)
        y = self.y + step_size * self.speed * sin(self.theta)
        if self.tank.finite:
            if self.tank.not_in_tank(x,y):
                return self.tank.bring_point_into_tank(x, y)
        return x, y


class FishTank:
    def __init__(self, size: float, random_seed: int = None) -> None:
        """Fish tank object to store fish in

        Args:
            size (float): size of tank
            random_seed (int, optional): seed integer for random number 
            generation. Defaults to None.
        """
        self.spawn_area = size
        self.fishes = []
        self.finite = False
        self.torus = False
        if isinstance(random_seed, int):
            seed(random_seed)

    def add_fish(self, fish: Zebrafish) -> None:
        """Stores ZebbraFish objects in the tank

        Args:
            fish (ZebbraFish): ZebbraFish object to be added to the tank
        """
        self.fishes.append(fish)

    def seed_fishes_randomly(self) -> None:
        """Seed every fish in the tank with a random position and random 
        intial direction
        """
        for fish in self.fishes:
            fish.seed_fish(self.spawn_area * random(),
                            self.spawn_area * random(),
                            tau * random(), self)

    def seed_fish_manually(self, x: list, y: list, angles: list) -> None:
        """Seed each fish in tank with a manually specified postion and initial direction

        Args:
            x (list[float]): list of x coordinates for each fish
            y (list[float]): list of y coordinates for each fish
            angles (list[float]): list of initial angles for each fish
        """
        for i, fish in enumerate(self.fishes):
            fish.seed_fish(x[i], y[i], angles[i], self)

    def step(self, step_size: float) -> None:
        """Steps all fish in tank a given time step

        Args:
            step_size (float): amount to step simulation
        """
        for fish in self.fishes:
            fish.adjust_course()
        for fish in self.fishes:
            fish.step(step_size)


class SqrtFishTank(FishTank):
    def __init__(self, side_lenght: float, random_seed: int = None) -> None:
        """An square fish tank extension to FishTank class. If fish leave the 
        tank they are brought back to the opposite side. "A fish tank on torus"

        Args:
            side_lenght (float): side lenght of the fish tank
            random_seed (int, optional): seed integer for random number 
            generation. Defaults to None.
        """
        FishTank.__init__(self, side_lenght, random_seed)
        self.finite = True
        self.torus = True

    def not_in_tank(self, x: float, y: float) -> bool:
        """Given an x and y coordinate checks whether the point in the fish 
        tank

        Args:
            x (float): x coordinate to check
            y (float): y coordinate to check

        Returns:
            bool: whether the point is not in the fish tank
        """
        if any([x, y < 0]) or any([x, y > self.spawn_area]):
            return True
        else:
            return False

    def bring_point_into_tank(self, x: float, y:float) -> tuple:
        """Given a point it returns it back into the fish tank

        Args:
            x (float): x coordinate to bring back into the tank
            y (float): y coordinate to bring back into the tank

        Returns:
            tuple[float float]: new coordinates
        """
        x %= self.spawn_area
        y %= self.spawn_area
        return x, y


class VisualSqrtFishTank(SqrtFishTank, Rectangle):
    def __init__(self, side_lenght: float, random_seed: int = None) -> None:
        ''' Visual version of the square tank that can be added to a 
        figure as a matplotlib patch '''
        SqrtFishTank.__init__(self, side_lenght, random_seed)
        Rectangle.__init__(self, (0, 0), side_lenght, side_lenght, fill= None)
