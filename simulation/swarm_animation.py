#!/usr/bin/env python3

from math import sin, cos

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
import numpy as np

from fish import Zebrafish, VisualSqrtFishTank

def main():
    # Simulation parameters
    TANK_SIZE = 50
    FISH_SPEED = 1
    DETECTION_RADIUS = 15
    ETA = 0.2
    DELTA_T = 0.5
    N = 200
    
    # Simulation objects
    global tank
    tank = VisualSqrtFishTank(TANK_SIZE)
    for _ in range(100):
        tank.add_fish(Zebrafish(FISH_SPEED, DETECTION_RADIUS, ETA))

    tank.seed_fishes_randomly()

    #Run Simulation
    for _ in range(N):
        tank.step(DELTA_T)

    global positions
    global angles
    positions = []
    angles = []

    # Generate array of positions and angles
    for frame in range(N):
        frame_positions = []
        frame_angles = []
        for fish in tank.fishes:
            frame_positions.append([fish.x_trajectory[frame], fish.y_trajectory[frame]])
            frame_angles.append(fish.theta_trajectory[frame])
        positions.append(frame_positions)
        angles.append(frame_angles)

    fig, ax = plt.subplots()
    global ln

    # generate initial quiver plot
    ln = ax.quiver([x[0] for x in positions[0]], [x[1] for x in positions[0]], 
        [cos(x) for x in angles[0]], [sin(x) for x in angles[0]], scale=20)
    # animiate quiver plt
    a = FuncAnimation(fig, animate, frames=len(positions), blit=True)
    plt.show()


def animate(idx_timestamp):
    # update plt with new data
    ln.set_offsets(positions[idx_timestamp])
    ln.set_UVC(np.cos(angles[idx_timestamp]), np.sin(angles[idx_timestamp]))
    return (ln,)



if __name__ == '__main__':
    main()
