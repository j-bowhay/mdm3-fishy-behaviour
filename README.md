# MDM3 - Fishy Behaviour

## Authors
Eden Page, Jake Bowhay, Wilson Wu, Stephen Powell, Patrick Shortall

## Structure

This repository contains all the data and code used in our project. The repository is split in half with one directory for all data and data processing and the other half containing all the code for the simulations. Please see the individual `README` in each directory for a detailed description of each section.