%
%   FIND MEAN SPEED OF FISH FROM DATA
%

clearvars;

% load in concatinated fish data
load('Fish_data.mat');

% get fieldnames so indexing is possible
fns = fieldnames(Fish_data);

% temp vector for average speed of each the 18 trails
temp_speeds = zeros(size(fns));

% loop through each trial
for i = 1:length(fns)
    % get speed vector for each fish
    fish1_speed = Fish_data.(fns{i})(1).speed;
    fish2_speed = Fish_data.(fns{i})(2).speed;
    
    % get position vector for each fish
    fish1_pos = Fish_data.(fns{i})(1).pos;
    fish2_pos = Fish_data.(fns{i})(2).pos;
    
    % remove data near wall
    fish1_speed(~dataWithinRadius(fish1_pos, 40)) = [];
    fish2_speed(~dataWithinRadius(fish2_pos, 40)) = [];

    
    % remove any invalid data
    fish1_speed = clean(fish1_speed);
    fish2_speed = clean(fish2_speed);    
    
    % get mean of the mean speed for each fish
    temp_speeds(i) = mean([mean(fish1_speed) mean(fish2_speed)]);
end

% get mean of all the trials
mean(temp_speeds)