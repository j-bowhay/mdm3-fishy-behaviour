# Data

## Original Data Set

The original experimental data is provided by A. Zienkiewicz et al. [1]. The data set contains data on 18 different 20 minute experiments where two zebrafish are swimming around a tank.

## Modified Data Set

We have modified the data set to include only the experimental data. This is contained in the `Fish_data.mat` file.

## Helper Functions

`clean.m` - Removes any `inf` or `Nan` values from data

`dataWithinRadius.m` - Generates logical array showing data points that are within a given radius of the centre of the tank. This is used to remove any data that is being influenced by the tank boundary.

## Data Processing

To optimise the parameters in our simulation we needed to generate the following summary statistics from the experimental data.

* Average speed (`speed.m`)
* Mean of the RMS angular velocity of each fish (`rms_angular_velocity.m`)
* Mean distance between the two fish (`distance.m`)
* Order/Alignment (`order_parameter.m`)

## References

[1] 	Mario Di Bernardo, David Barton, Adam Zienkiewicz (2015): Leadership emergence in a data-driven model of zebrafish shoals with speed modulation. https://doi.org/10.5523/bris.jwp3t4c6ioj41mfkrmsl2vgt6