%
%   FIND MEAN RMS ANGULAR VELOCITY OF FISH FROM DATA
%


clearvars;

% load in fish data
load('Fish_data.mat');

% get fieldnames so indexing is possible
fns = fieldnames(Fish_data);

% temp vector for RMS angular velocity of each the 18 trails
temp_rms = zeros(size(fns));

% loop through each trial
for i = 1:length(fns)
    % get angular vector for each fish
    fish1 = Fish_data.(fns{i})(1).omega_vel;
    fish2 = Fish_data.(fns{i})(2).omega_vel;
    
    % get position vector for each fish
    fish1_pos = Fish_data.(fns{i})(1).pos;
    fish2_pos = Fish_data.(fns{i})(2).pos;
    
    % remove data near wall
    fish1(~dataWithinRadius(fish1_pos, 40)) = [];
    fish2(~dataWithinRadius(fish2_pos, 40)) = [];
    
    % remove any invalid data
    fish1 = clean(fish1);
    fish2 = clean(fish2);
    
    % calculate the mean RMS angular velocity of the two fish
    temp_rms(i) = mean([sqrt(mean(fish1.^2)) sqrt(mean(fish2.^2))]);
end

% get the mean of all the trials
deg2rad(mean(temp_rms))