function data = clean(data)
    % Removes any NaN or inf values from the vector
    data = data( ~any(isnan(data) | isinf(data), 2),:);
end

