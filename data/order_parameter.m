%
%   FIND Order BETWEEN FISH FROM DATA
%

clearvars;

% load in fish data
load('Fish_data.mat');

% get fieldnames so indexing is possible
fns = fieldnames(Fish_data);

% temp vector for distance between fish of each the 18 trails
temp_dist = zeros(size(fns));

% loop through each trial
for i = 1:length(fns)
    % get the position of each fish
    fish1 = Fish_data.(fns{i})(1).vel;
    fish2 = Fish_data.(fns{i})(2).vel;
    
    % get position vector for each fish
    fish1_pos = Fish_data.(fns{i})(1).pos;
    fish2_pos = Fish_data.(fns{i})(2).pos;
    
    % check that there is valid data for both fish
    to_keep = ~any(isnan(fish1) | isinf(fish1), 2) & ...
        ~any(isnan(fish2) | isinf(fish2), 2);
    
    % remove data near wall
    within_radius = dataWithinRadius(fish1_pos, 40) & dataWithinRadius(fish2_pos, 40);
    
    % remove any invalid data
    fish1 = fish1(to_keep & within_radius,:);
    fish2 = fish2(to_keep & within_radius,:);
    
    % Normalize
    fish1 = fish1./vecnorm(fish1,2,2);
    fish2 = fish2./vecnorm(fish2,2,2);
    
    temp_dist(i) = mean(0.5*vecnorm(fish1 + fish2, 2, 2),'omitnan');
end

mean(temp_dist)