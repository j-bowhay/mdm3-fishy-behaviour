function to_keep = dataWithinRadius(pos, radius)
    %Finds all the data within a given radius from the center of the tank
    %Returns logical array
    dist = vecnorm(pos,2,2);
    to_keep = dist < radius;
end

